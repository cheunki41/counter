import "./CounterGroupSum";

const CounterGroupSum = ({ sum }) => {
  return (
    <div className="CounterGroupSum">
      <p>Sum: {sum}</p>
    </div>
  );
};

export default CounterGroupSum;
