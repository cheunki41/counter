import Counter from "./Counter";

const CounterGroup = ({ counterList, update }) => {
  return (
    <div>
      {counterList.map((number, index) => (
        <Counter
          key={index}
          number={number}
          update={(action) => update(action, index)}
        />
      ))}
    </div>
  );
};

export default CounterGroup;
