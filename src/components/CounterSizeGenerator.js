import "./CounterSizeGenerator.css";

const CounterSizeGenerator = ({ counterSize, setCounterSize, reset }) => {
  const handleCounterSizeChange = (event) => {
    setCounterSize(event.target.value);
    reset(event.target.value);
  };

  return (
    <div className="CounterSizeGenerator">
      <label htmlFor="counterSize">size:</label>
      <input
        type="number"
        min="0"
        name="counterSize"
        id="counterSize"
        value={counterSize}
        onChange={handleCounterSizeChange}
      />
    </div>
  );
};

export default CounterSizeGenerator;
