import { useState } from "react";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroupSum from "./CounterGroupSum";
import CounterGroup from "./CounterGroup";
import "./MultipleCounter.css";

const MultipleCounter = () => {
  const [counterSize, setCounterSize] = useState(0);
  const [sum, setSum] = useState(0);
  const [counterList, setCounterList] = useState([]);

  const update = (action, index) => {
    counterList[index] += action;
    setCounterList(counterList);
    setSum(sum + action);
  };

  const reset = (counterSize) => {
    setCounterList(Array.from({ length: counterSize }).fill(0));
    setSum(0);
  };

  return (
    <div className="MultipleCounter">
      <CounterSizeGenerator
        counterSize={counterSize}
        setCounterSize={setCounterSize}
        reset={reset}
      />
      <CounterGroupSum sum={sum} />
      <CounterGroup counterList={counterList} update={update} />
    </div>
  );
};

export default MultipleCounter;
