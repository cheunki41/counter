const Counter = ({ update, number }) => {
  const add = () => {
    update(1);
  };

  const minus = () => {
    update(-1);
  };

  return (
    <div>
      <button onClick={add}> + </button>
      <span> {number} </span>
      <button onClick={minus}> - </button>
    </div>
  );
};

export default Counter;
